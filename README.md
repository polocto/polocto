```
     ___           ___           ___       ___       ___
    /\__\         /\  \         /\__\     /\__\     /\  \
   /:/  /        /::\  \       /:/  /    /:/  /    /::\  \
  /:/__/        /:/\:\  \     /:/  /    /:/  /    /:/\:\  \
 /::\  \ ___   /::\~\:\  \   /:/  /    /:/  /    /:/  \:\  \
/:/\:\  /\__\ /:/\:\ \:\__\ /:/__/    /:/__/    /:/__/ \:\__\
\/__\:\/:/  / \:\~\:\ \/__/ \:\  \    \:\  \    \:\  \ /:/  /
     \::/  /   \:\ \:\__\    \:\  \    \:\  \    \:\  /:/  /
     /:/  /     \:\ \/__/     \:\  \    \:\  \    \:\/:/  /
    /:/  /       \:\__\        \:\__\    \:\__\    \::/  /
    \/__/         \/__/         \/__/     \/__/     \/__/
```
```
      ___            ___                 ___            ___            ___            ___
     /\  \         /:\  \               /\  \          /\  \          /\__\          /\__\
     \:\  \   /\  /: :|  |             /::\  \        /::\  \        /:/  /         /:/  /
      \:\  \ | / /:/|:|  |            /:/\:\  \      /:/\:\  \      /:/  /         /:/  /
  ___ /::\  \\/ /:/ |:|__|___        /::\~\:\  \    /::\~\:\  \    /:/  /  ___    /:/  /
 /\  /:/\:\__\ /:/ /|:_:_:\__\      /:/\:\ \:\__\  /:/\:\ \:\__\  /:/__/  /\__\  /:/__/
 \:\/:/ /\/__/ \/_/     /:/  /      \/__\:\/:/  /  \/__\:\/:/  /  \:\  \ /:/  /  \:\  \
  \::/ /               /:/  /            \::/  /        \::/  /    \:\  /:/  /    \:\  \
   \:\  \             /:/  /              \/__/         /:/  /      \:\/:/  /      \:\  \
    \:\__\           /:/  /                            /:/  /        \::/  /        \:\__\
     \/__/           \/__/                             \/__/          \/__/          \/__/

I am an IT engeneering student at ECE Paris Lyon.
```
## 🙋‍♂️ About Me
---
- 💻‍ I'm currently graduate from  my schools [ECE Paris-Lyon](https://www.ece.fr/) where I was in the major **informatics systems**, **cyber-security** and **big data**, and [ETS](https://www.etsmtl.ca/) in the Genie des TI program. I was taught about operating system, data base, operational development, computer networks, web technologies, devops, logiciels development, computer vision, ray tracing.
- 🔭 You will find in my [repositories](https://gitlab.com/users/polocto/projects) some of my school projects 
- ⚡ Fun fact: I made an exchange semester at [Ajou University](https://www.ajou.ac.kr/en/index.do) in South Korea.

## 🤖 Languages & Tools
---
![C](./img/languages&tools/c-programming.png)
![C++](./img/languages&tools/c++.png) 
![Java](./img/languages&tools/java.png) 
![HTML](./img/languages&tools/html-5.png) 
![CSS](./img/languages&tools/css3.png) 
![JavaScript](./img/languages&tools/javascript.png)
![Python](./img/languages&tools/python.png)
![YAML](./img/languages&tools/yaml.png)
![NodeJS](./img/languages&tools/nodejs.png) 
![Linux](./img/languages&tools/linux.png) 
![Windows](./img/languages&tools/windows-11.png)
![Vagrant](./img/languages&tools/vagrant.png) 
![Docker](./img/languages&tools/docker.png)
![K8s](./img/languages&tools/kubernetes.png)
![Redis](./img/languages&tools/redis.png)
![Github](./img/github.png)
![Gitlab](./img/gitlab.png)

## Connect with me
---
[![LinkedIn](./img/social/linkedin.png)](https://www.linkedin.com/in/paul-sade-a6222448/)
[![Github](./img/github.png)](https://github.com/polocto)
[![Gitlab](./img/gitlab.png)](https://gitlab.com/polocto)
[![Facebook](./img/social/facebook.png)](https://www.facebook.com/profile.php?id=100008280398252)
[![Mail](./img/social/mail.png)](mailto:paul.sade@live.fr)
[![Telegram](./img/social/telegram.png)](https://t.me/polocto)
